# Build container images

Repo with container images to use on Gitlab CI.

## Testing the builds locally

    for I in aptly bookworm-libpf rsync smtp-remote skopeo postgres-en-gb
    do
      docker build . -f "${I}/Dockerfile"
    done