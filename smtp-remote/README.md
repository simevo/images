smtp_remote
===========

Listens on port 25 locally and transfers everything to a remote SMTP host.

Create the file `passwd.client` with authentication credentials in the form `<server>:<username>:<user password>` (e.g. `server.example.com:mymail@example.com:abdc1243`).

Start the service with: `docker run --hostname simevo.com --rm -p 25:25 -it -v "$PWD/secrets/passwd.client:/etc/exim4/passwd.client" registry.gitlab.com/simevo/images/smtp-remote` then send a test email with:

    echo "My message" | mail -r noreply@simevo.com -s subject john.doe@example.com
